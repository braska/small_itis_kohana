$(document).ready(function() {
    $('form#registration').on('submit', function (e) {
        if ($('form#registration input#login').val().length == 0) {
            e.preventDefault();
            alert('Заполните поле "Логин"');
        }
        if ($('form#registration input#password').val().length < 6) {
            e.preventDefault();
            alert('Пароль должен быть не короче 6 символов');
        }
    });
});