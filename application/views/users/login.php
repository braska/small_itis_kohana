<h1>Вход</h1>

<?php if (isset($messages)):?>
    <?php foreach($messages as $message):?>
        <div class="alert alert-danger">
            <?=$message?>
        </div>
    <?php endforeach; ?>
<?php endif;?>

<form class="form-horizontal" method="post" id="form-login">
    <div class="form-group">
        <label for="login" class="col-sm-2 control-label">Логин</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="username" id="login">
        </div>
    </div>

    <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Пароль</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="password">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Войти</button>
        </div>
    </div>
</form>