<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users extends Controller_Template {
    public $template = "layouts/index";

    public function action_registration()
    {
        $view = View::factory("users/registration");

        if ($this->request->method() == HTTP_Request::POST) {
            try
            {
                $user = ORM::factory('User');
                $user->username = $this->request->post("username");
                $user->email = $this->request->post("email");
                $user->password = $this->request->post("password");
                $user->save();
                $user->add('roles', array(1));
                $view->messages = array('Вы успешно зарегистрировлись!');
            }
            catch (ORM_Validation_Exception $e)
            {
                $errors = $e->errors('validation');
                $view->messages = array();
                foreach ($errors as $error) $view->messages[] = $error;
            }
        }

        $this->template->content = $view;
    }

    public function action_login()
    {
        $view = View::factory("users/login");

        if ($this->request->method() == HTTP_Request::POST) {
            if (Auth::instance()->login(
                $this->request->post("username"),
                $this->request->post("password")
            )) {
                $this->redirect('/');
            } else {
                $view->messages = array('Вы ввели неверный логин или пароль');
            }
        }

        $this->template->content = $view;
    }

    public function action_logout() {
        Auth::instance()->logout();
        $this->redirect('/');
    }
}
