<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller_Template {
    public $template = "layouts/index";

    public function action_index()
    {
        $view = View::factory("news/index");

        $news = ORM::factory('News');
        $count = $news->count_all();
        $rows = $news->get_all_news();

        $view->rows = $rows;
        $view->count = $count;

        $this->template->content = $view;
    }

    public function action_show()
    {
        $view = View::factory("news/show");
        $id = $this->request->param('id');

        $news = ORM::factory('News', $id);
        $view->news = $news;

        $this->template->content = $view;
    }
}
