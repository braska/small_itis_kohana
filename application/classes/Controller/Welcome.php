<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {
	public $template = "layouts/index";

	public function action_index()
	{
		$view = View::factory("welcome/index");
		$this->template->content = $view;
	}

}
